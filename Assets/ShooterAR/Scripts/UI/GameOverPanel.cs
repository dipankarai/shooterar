﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

namespace ShooterAR
{
    public class GameOverPanel : MonoBehaviour
    {

        // Use this for initialization
        void Start()
        {

        }
        
        public void OnClickRestartButton()
        {
            SceneManager.LoadScene("GamePlay");
        }

        public void OnClickQuitButton()
        {
            Application.Quit();
        }
    }
}
