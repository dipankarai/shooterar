﻿using UnityEngine;
using System.Collections;
using CnControls;

namespace ShooterAR
{
    public class PlayerController : MonoBehaviour
    {
        public float speed = 2f;

        // Use this for initialization
        void Start()
        {

        }

        void FixedUpdate()
        {
            Move(CnInputManager.GetAxis("Horizontal"));        
        }

        void Move(float move)
        {
            Vector3 playerPos = transform.position;
            playerPos.x += (move * Time.deltaTime) * speed;
            transform.position = playerPos;
        }
    }
}
