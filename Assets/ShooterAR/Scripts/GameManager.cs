﻿using UnityEngine;
using System.Collections;

namespace ShooterAR
{
    public class GameManager : MonoBehaviour
    {
        #region INIT Instance
        private static GameManager instance;
        public static GameManager Instance
        {
            get
            {
                return instance;
            }
        }

        void InitInstance()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                DestroyImmediate(this);
            }
        }
        #endregion

        public GameObject player;

        private GameScore gameScore;
        public UIHandlerGameplay uIHandlerGameplay;
        public bool isGameOver = false;

        void Awake()
        {
            InitInstance();
        }

        // Use this for initialization
        void Start()
        {
            if (gameScore == null)
            {
                gameScore = new GameScore();
            }

            InitHUD();
        }

        public void EnemyKilled ()
        {
            gameScore.EnemyKilled();
            uIHandlerGameplay.UpdateScore(gameScore.TotalScore);
        }

        public void PlayerKilled()
        {
            if (gameScore.IsGameOver())
            {
                //GameOver.....
                isGameOver = true;
                uIHandlerGameplay.UpdateScore(gameScore.TotalScore);
                uIHandlerGameplay.GameOver();
            }
            else
            {
                //Decrese player Life
                uIHandlerGameplay.UpdatePlayerLife(gameScore.PlayerLife);
                uIHandlerGameplay.UpdateScore(gameScore.TotalScore);
            }
        }

        void InitHUD ()
        {
            if (uIHandlerGameplay == null)
                uIHandlerGameplay = FindObjectOfType<UIHandlerGameplay>();

            uIHandlerGameplay.UpdatePlayerLife(gameScore.PlayerLife);
            uIHandlerGameplay.UpdateScore(gameScore.TotalScore);
        }
    }
}
